# Configure BFFH Log
BFFH's log level can be set via the environment variables or via bffh.dhall.

`BFFH_LOG=debug`

Supported levels are:
* `trace`
* `debug`
* `warn`
* `info`
* `error`
