# Audit Log
Bffh will log state changes into the audit log file, one per line.

Audit log entries are for now JSON:
`{"timestamp":1641497361,"machine":"Testmachine","state":{"state":{"InUse":{"uid":"Testuser","subuid":null,"realm":null}}}}`
